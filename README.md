# ⚙️ .dotfiles

A bunch of my .dotfiles/ configuration for some tools I use daily to improve my productivity as a developer. This repo also serves as a collection that can be easily checked to help obtain a consistent workstation feel among different machines.

## 🌟 Main Features

* [neovim](https://github.com/neovim/neovim)
* [tmux](https://github.com/tmux/tmux)
* Aliases
* neovim as the MANPAGER. Your manual pages have _never_ looked sexier!
* neovim as the EDITOR. e.g. when writing a commit message.
* [_secret_](https://piped.kavin.rocks/watch?v=dQw4w9WgXcQ)

## 🛠️ Troubleshooting

* [vim-plug](https://github.com/junegunn/vim-plug): it manages the plugins on neovim and needs to be installed.
* Node: it must be installed and be up to date, as it is required by one of the vim plugins, [coc.nvim](https://github.com/neoclide/coc.nvim).
